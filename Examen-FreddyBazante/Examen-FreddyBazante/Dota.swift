//
//  Dota.swift
//  Examen-FreddyBazante
//
//  Created by Freddy Bazante on 12/10/17.
//  Copyright © 2017 Freddy Bazante. All rights reserved.
//

import Foundation
import ObjectMapper

class Dota:Mappable {
    
    var dtId:Int?
    var dtName:String?
    
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        dtId <- map["ID"]
        dtName <- map["Name"]
       
    }
}
