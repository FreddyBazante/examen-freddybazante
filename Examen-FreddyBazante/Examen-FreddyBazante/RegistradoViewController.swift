//
//  RegistradoViewController.swift
//  Examen-FreddyBazante
//
//  Created by Freddy Bazante on 12/9/17.
//  Copyright © 2017 Freddy Bazante. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class RegistradoViewController: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
            
    //MARK:- ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    //MARK:- Actions
    @IBAction func consultarButtonPressed(_ sender: Any) {
        let number = Int(arc4random_uniform(111)+1)
        Alamofire.request("http://api.herostats.io/heroes/\(number)").responseObject { (response: DataResponse<Dota>) in
            
            let heroe = response.result.value
            
            DispatchQueue.main.async {
                self.idLabel.text = "\(heroe?.dtId)"
                self.nameLabel.text = heroe?.dtName
            }
        }
    }
    @IBAction func salirButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
