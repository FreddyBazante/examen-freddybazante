//
//  InvitadoViewController.swift
//  Examen-FreddyBazante
//
//  Created by Freddy Bazante on 12/9/17.
//  Copyright © 2017 Freddy Bazante. All rights reserved.
//

import UIKit
import CoreLocation
class InvitadoViewController: UIViewController, CLLocationManagerDelegate {

    let locationManager = CLLocationManager()
    var didGetWheather = false
    
    //MARK:- Outlets
    @IBOutlet weak var weatherLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
        }
   }
    
    //MARK:- CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = manager.location?.coordinate
        if !didGetWheather {
            getWeatherByLocation(
                lat: (location?.latitude)!,
                lon: (location?.longitude)!
            )
            didGetWheather = true
        }
    }
    
    //MARK:- Actions
    @IBAction func salirButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    private func getWeatherByLocation(lat:Double, lon:Double){
        let servicio = Servicio()
        servicio.getWeatherByLocation(lat: lat, lon: lon) { (weather) in
            DispatchQueue.main.async {
                self.weatherLabel.text = weather
            }
        }
    }
    
}
