//
//  Servicio.swift
//  Examen-FreddyBazante
//
//  Created by Freddy Bazante on 12/10/17.
//  Copyright © 2017 Freddy Bazante. All rights reserved.
//

import Foundation

class Servicio {
    
    func getWeatherByLocation(lat:Double, lon:Double, completion: @escaping (String) -> () ){
        let urlStr = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=7d95e6fefc2b7875252004fead0caaeb"
        
        getWeather(urlString: urlStr) { (weather) in
            completion(weather)
        }
    }
    
    private func getWeather(urlString:String, completion: @escaping (String) -> ()){
        
        let url = URL(string: urlString)
        let request = URLRequest(url: url!)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let _ = error {
                return
            }
            let weatherData = data as! Data
            do {
                let weatherJson = try JSONSerialization.jsonObject(with: weatherData, options: [])
                let weatherDictionary = weatherJson as! NSDictionary
                let weatherArray = weatherDictionary["weather"] as! NSArray
                let weather = weatherArray[0] as! NSDictionary
                let weatherDesc = weather["description"] ?? "error"
                completion(weatherDesc as! String)
            } catch {
                print("Error al parcear el Json")
            }
        }
        task.resume()
    }
    
}
